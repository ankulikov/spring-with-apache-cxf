package edu.kulikov.filters;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

public class MediaTypeProducerFilter extends OncePerRequestFilter {
    public static final String MEDIA_TYPE = "mediaType";
    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws ServletException, IOException {
        String reqType = req.getParameterMap().containsKey("type") ? req.getParameter("type") : "xml";
        MediaType resType;

        switch (reqType) {
            case "json":
                resType = MediaType.APPLICATION_JSON_TYPE;
                break;
            case "xml":
            default:
                resType = MediaType.APPLICATION_XML_TYPE;
                break;

        }
        getServletContext().setAttribute(MEDIA_TYPE, resType);
        chain.doFilter(req, res);
    }
}
