package edu.kulikov.rest;

import edu.kulikov.dao.BookDao;
import edu.kulikov.filters.MediaTypeProducerFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("books")
@Service("bookService")
public class BookService {
    @Autowired
    BookDao bookDao;
    @Context
    ServletContext context;

    @GET
    @Produces({"application/json", "application/xml"})
    @Path("/{id}")
    public Response getBook(@PathParam("id") Integer id) {
        return Response.ok(bookDao.getBook(id),
                (MediaType) context.getAttribute(MediaTypeProducerFilter.MEDIA_TYPE)).build();

    }
}
