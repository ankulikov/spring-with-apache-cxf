package edu.kulikov.dao;

import edu.kulikov.models.Book;
import org.springframework.stereotype.Repository;

@Repository
public class BookDaoImpl implements BookDao {
    public Book getBook(int id) {
        Book book = new Book();
        book.setId(id);
        book.setAuthor("Andrey");
        book.setPrice(15);
        book.setTitle("Spring+Apache CFX");
        return book;
    }
}
