package edu.kulikov.dao;

import edu.kulikov.models.Book;

public interface BookDao {
    public Book getBook(int id);
}
